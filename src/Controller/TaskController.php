<?php

namespace App\Controller;

use App\Entity\Task;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TaskController extends AbstractController
{
    private const STATUS_BACKLOG = 'backlog';
    private const STATUS_ACTIVE = 'active';
    private const STATUS_DONE = 'done';
    private const STATUS_TEST = 'test';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/task", name="task_create", methods="POST")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws Exception
     */
    public function createTask(Request $request, ValidatorInterface $validator)
    {
        $taskData = json_decode($request->getContent(), true);

        $task = new Task();

        if (!isset($taskData['name'], $taskData['description'], $taskData['deadline'])) {
            return $this->json([
                'success' => false,
                'errors' => 'Переданы не все поля'
            ]);
        }
        $task->setName($taskData['name'])
            ->setDescription($taskData['description'])
            ->setStatus($taskData['status'])
            ->setDeadline(new DateTimeImmutable($taskData['deadline']));

        $errors = $validator->validate($task);
        if (count($errors) > 0) {
            return $this->json([
                'success' => false,
            ]);
        }

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->json([
            'success' => true,
        ]);
    }

    /**
     * @Route("/task/{id}", name="task_get", methods="GET")
     * @param int $id
     * @return JsonResponse
     */
    public function getTask(int $id)
    {
        $task = $this->entityManager->find(Task::class, $id);

        if (!$task) {
            return $this->json([
                'success' => false,
                'errors' => 'Передан неверный ID задачи'
            ]);
        }

        return $this->json([
            'name' => $task->getName(),
            'description' => !is_null($task->getDescription()) ?: "",
            'status' => $task->getStatus(),
            'deadline' => $task->getDeadline(),
        ]);
    }

    /**
     * @Route("/task/{id}", name="task_delete", methods="DELETE")
     * @param int $id
     * @return JsonResponse
     */
    public function deleteTask(int $id)
    {
        $task = $this->entityManager->find(Task::class, $id);

        if (!$task) {
            return $this->json([
                'success' => false,
                'errors' => 'Передан неверный ID задачи'
            ]);
        }

        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return $this->json([
            'status' => true,
        ]);
    }

    /**
     * @Route("/task/{id}", name="task_update", methods="PUT")
     * @param int $id
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function updateTask(int $id, Request $request, ValidatorInterface $validator)
    {
        $task = $this->entityManager->find(Task::class, $id);
        if (!$task) {
            return $this->json([
                'success' => false,
                'errors' => 'Передан неверный ID задачи'
            ]);
        }

        $taskData = json_decode($request->getContent(), true);

        if (isset($taskData['name'])) {
            $task->setName($taskData['name']);
        }
        if (isset($taskData['description'])) {
            $task->setDescription($taskData['description']);
        }
        if (isset($taskData['deadline'])) {
            $task->setDeadline($taskData['deadline']);
        }

        $errors = $validator->validate($task);
        if (count($errors) > 0) {
            return $this->json([
                'success' => false,
            ]);
        }

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->json([
            'status' => true,
        ]);
    }
}
